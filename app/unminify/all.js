var inputEmail = document.getElementById('email');

inputEmail.onfocus = function() {
  this.classList.add('form-group__input_active');
}

inputEmail.onblur = function() {
  if (this.value === "") {
    this.classList.remove('form-group__input_active');
  }
}
